const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const mongoose = require('mongoose');
const User = require('../models/user');
const multer = require('multer');
const assert = require('assert');
// const Photo = mongoose.model('Photos');
//register 
//connect to database


mongoose.connect(config.database);

const conn = mongoose.connection;


router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        // img : req.body.img
    });

    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, msg: 'failed to register user' });
        } else {
            res.json({ success: true, msg: 'User Registred' });
        }
    })
});


//To AUTHANTICATE
router.post('/authenticate', (req, res, next) => {

    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {

        if (err) throw err;
        if (!user) {
            return res.json({ success: false, msg: 'User not Found' });
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 804800 // 1 week
                });
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email
                    }
                });
            } else {
                return res.json({ success: false, msg: 'wrong password' })
            }
        })
    })


});

//To Get the Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({ user: req.user });
});

//To upadte the User Detailas
router.patch('/update', (req, res, next) => {
    const username = req.body.username;
    User.update({ username }, {
        $set: {
            name: req.body.name,
            email: req.body.email
        }
    }, (err, result) => {
        if (err) {
            res.json({ success: false, msg: 'failed to Update user' });
        } else {
            res.send(result);
            console.log(result);
            // res.json({ success: true, msg: 'User Updated' });
        }
    })

});

// profile upload 


///Storage for Image
let storage = multer.diskStorage({
    //Indicates where you want to save your files
    destination: (req, file, callback) => {
        callback(null, 'public/uploads/images');
    },
    filename: (req, file, callback) => {

        callback(null, file.fieldname + "-" + file.originalname);

    }
    //For crypto file name
    // crypto.pseudoRandomBytes(16, (err, raw) =>{
    //     if (err) return callback(err);
    //     callback(null, raw.toString('hex') + path.extname(file.originalname));
    //   });
});
let upload = multer({ storage: storage });

router.get('/api', function (req, res) {
    res.send('file catcher example');
});

router.post('/profileimg', upload.single('image'), (req, res, next) => {
    mongoose.connect(config.database, (err, conn) => {
        assert.equal(null, err);
        insertDocuments(conn, 'public/uploads/images' + req.file.filename, () => {
            res.json({'message': 'File uploaded successfully'});
        });
    });
});

let insertDocuments = (conn, filePath, callback) => {
    let collection = conn.collection('files');
    collection.insertOne({'imagePath' : filePath }, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
}


module.exports = router;