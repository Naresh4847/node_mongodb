const express = require('express');
const path = require('path');
const bodyparser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const http = require('http');
const config = require('./config/database');
const cookieParser = require('cookie-parser');
const fs = require('fs');


// For Routing We Import The Routers
const users = require('./routes/users');

//
const conn = mongoose.connection;

//connect to database
mongoose.connect(config.database);

//On Connection
conn.on('connected', () => {
    console.log('connected to database' + config.database);

});
//On error
conn.on('error', (err) => {
    console.log('database error' + err);
});

const app = express();

//port number
const port = 3000;

//middleware
app.use(cors())

app.use(bodyparser.urlencoded({ extended: false }));

app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyparser.json());
app.use(passport.initialize());

app.use(passport.session());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//intializing for passport
require('./config/passport')(passport);
app.use('/users', users);

//index route
app.get('/', (req, res) => {
    res.send("invalid endpoint");
});

app.listen(port, (err)=>{
    console.log('Express server listening on port '+ port);
})


